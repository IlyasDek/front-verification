import {Component, Input, OnInit} from '@angular/core';
import {NzModalRef, NzNotificationService} from 'ng-zorro-antd';
import {AlarmService} from '../../../../../services/alarm.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {VALID} from '../../../../../models/Constant';

@Component({
  selector: 'app-m-submit',
  templateUrl: './m-submit.component.html',
  styleUrls: ['./m-submit.component.scss']
})
export class MSubmitComponent implements OnInit {

  @Input() activate: any;
  validateForm: FormGroup;
  captcha: any;
  code = false;

  constructor(private modal: NzModalRef,
              private notification: NzNotificationService,
              private fb: FormBuilder,
              private alarmService: AlarmService) {
  }

  ngOnInit() {
    this.validateForm = this.fb.group({
      verificationCode: [null, [Validators.required]],
      answer: [null],
    });
  }

  destroyModal() {
    this.modal.destroy();
  }

  submitForm() {
    // tslint:disable-next-line:forin
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

    if (this.validateForm.status === VALID) {
      const form = this.validateForm.getRawValue();
      form.email = this.activate.email;
      if (this.captcha) {
        console.log('captcha', this.captcha);
        form.questionId = this.captcha.questionId;
        form.personId = this.activate.id;
        this.alarmService.captcha(form).then(response => {
          this.captcha = response;
          this.notification.success('Вы купили товар', '');
        }).catch(() => {
          this.notification.error('Ошибка при покупке товара', '');
        });
      } else {
        this.alarmService.verify(form).then(response => {
          this.captcha = response;
          this.notification.success('Заявка отправлена', '');
        }).catch(() => {
          this.notification.error('Ошибка при отправке заявки', '');
        });
      }
    }
  }
}
